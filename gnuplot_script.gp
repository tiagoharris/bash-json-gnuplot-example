##
# gnuplot script to generate a graphic.
#
# it expects two parameters:
#
# csv_file_path - path to the file from which the data will be read
# graphic_file_name - the graphic file name to be saved 
#
# Author: Tiago Melo (tiagoharris@gmail.com)
##

# graphic will be saved as 800x600 png image file
set terminal png size 800,600

# setting the graphic file name to be saved
set output graphic_file_name

# allows grid lines to be drawn on the plot
set grid

# since the input file is a CSV file, we need to tell gnuplot that data fields are separated by comma
set datafile separator ","

# tells gnuplot that the values in X axis are date/time 
set xdata time

# tells gnuplot the datetime format of the data present in the input file
# all datetime values are in ISO 8601 format. For example: "2019-02-25T09:55:15.347+0000" 
set timefmt "%Y-%m-%dT%H:%M:%SZ"

# the graphic's main title
# we're appending the current date to it, in GMT-3 timezone
set title "Offer Metrics - ".strftime("%Y-%m-%d", time(0)-(3*3600))

# draws a box around the legends that we'll use...
set key box

# ... and place it in the upper right corner
set key right

# in the next three lines we are defining the style of the lines, where:
#
# lc - linecolor
# lt - linetype
# lw - linewidth
# pt - pointtype
# pt - pointinterval
# ps - pointsize
set style line 1 lc rgb '#4bd648' lt 1 lw 2 pt 7 pi -1 ps 1.5
set style line 2 lc rgb '#127ef3' lt 1 lw 2 pt 7 pi -1 ps 1.5
set style line 3 lc rgb '#e5532e' lt 1 lw 2 pt 7 pi -1 ps 1.5

# this is the command to actually generate the graphic file.
# the sintax is:
#
# plot <datafile> using <entry_in_file:entry_in_file> title <desired_title> with <plotting_style> ls <line_style>
#
# a line in the datefile has the following format, for example:
#
# 2019-02-25T09:55:15.347+0000,202,392,84
#
# the entry # 1 is a timestamp
# the entry # 2 is the number of new offers
# the entry # 3 is the number of updated offers
# the entry # 4 is the number of deleted offers
# 
plot \
csv_file_path using 1:2 title 'New offers' with linespoints ls 1, \
csv_file_path using 1:3 title 'Updated offers' with linespoints ls 2, \
csv_file_path using 1:4 title 'Deleted offers' with linespoints ls 3
