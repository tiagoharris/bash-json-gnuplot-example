#!/usr/bin/env bash

##
# bash script that parses a JSON file to CSV (using jq) and plots a graphic (using gnuplot).
#
# Author: Tiago Melo (tiagoharris@gmail.com)
##

LOG_FILE="./metrics.log"
CSV_FILE="./generated.csv"
GNUPLOT_SCRIPT_FILE="./gnuplot_script.gp"
GNUPLOT_GRAPHIC_FILE="./offer_metrics.png"

function generateCsvFile {
  cat $LOG_FILE | jq -r 'to_entries|map(.value)|@csv' | tr -d '"' > $CSV_FILE
}

function generateGraphic {
  gnuplot -e "csv_file_path='$CSV_FILE'" -e "graphic_file_name='$GNUPLOT_GRAPHIC_FILE'" $GNUPLOT_SCRIPT_FILE 
}

generateCsvFile
generateGraphic
exit
