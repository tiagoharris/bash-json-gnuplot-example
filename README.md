# batch-json-gnuplot-example

A bash script that plots a line graph from a JSON file using Gnuplot.

## Stack:
* [Gnuplot](http://www.gnuplot.info/)
* [jq](https://stedolan.github.io/jq/)


